import { CarpPage } from './app.po';

describe('carp App', () => {
  let page: CarpPage;

  beforeEach(() => {
    page = new CarpPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
